<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\Media;
use AppBundle\Form\MediaType;

/**
 * Project controller.
 *
 * @Route("/admin/media")
 */
class MediaController extends Controller
{

    /**
     * Lists all media entities.
     *
     * @Route("/", name="media_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $medias = $em->getRepository('AppBundle:Media')->findAll();

        return $this->render('media/index.html.twig', array(
            'medias' => $medias,
        ));
    }

    /**
     * @Route("/new", name="media_new")
     */
     public function newAction(Request $request)
     {
         $em = $this->getDoctrine()->getManager();
         $media = new Media();
         $form = $this->createForm('AppBundle\Form\MediaType', $media);
         $form->handleRequest($request);

         if ($form->isSubmitted() && $form->isValid()) {
             // $file stores the uploaded PDF file
             /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
             $file = $media->getDoc();

             // Generate a unique name for the file before saving it
             $fileName = md5(uniqid()).'.'.$file->guessExtension();

             // Move the file to the directory where brochures are stored
             $file->move(
                 $this->getParameter('docs_directory'),
                 $fileName
             );
             $em->persist($media);
             // Update the 'brochure' property to store the PDF file name
             // instead of its contents
             $media->setDoc($fileName);
             $em->flush();

             // ... persist the $media variable or any other work
             return $this->redirect($this->generateUrl('media_index'));
         }

         return $this->render('media/new.html.twig', array(
             'form' => $form->createView(),
         ));
     }


     /**
      * Displays a form to edit an existing media entity.
      *
      * @Route("/{id}/edit", name="media_edit")
      * @Method({"GET", "POST"})
      */
     public function editAction(Request $request, Media $media)
     {
         $em = $this->getDoctrine()->getManager();
         $deleteForm = $this->createDeleteForm($media);
         $editForm = $this->createForm(MediaType::class, $media);
         $editForm->handleRequest($request);

         if ($editForm->isSubmitted() && $editForm->isValid()) {
             // $file stores the uploaded PDF file
             /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
             $file = $media->getDoc();

             // Generate a unique name for the file before saving it
             $fileName = md5(uniqid()).'.'.$file->guessExtension();

             // Move the file to the directory where brochures are stored
             $file->move(
                 $this->getParameter('docs_directory'),
                 $fileName
             );
             $em->persist($media);
             // Update the 'brochure' property to store the PDF file name
             // instead of its contents
             $media->setDoc($fileName);
             $em->flush();

             return $this->redirectToRoute('media_edit', array('id' => $media->getId()));
         }

         return $this->render('media/edit.html.twig', array(
             'media' => $media,
             'edit_form' => $editForm->createView(),
             'delete_form' => $deleteForm->createView(),
         ));
     }

     /**
      * Finds and displays a media entity.
      *
      * @Route("/{id}", name="media_show")
      * @Method("GET")
      */
     public function showAction(Media $media)
     {
         $deleteForm = $this->createDeleteForm($media);

         return $this->render('media/show.html.twig', array(
             'media' => $media,
             'delete_form' => $deleteForm->createView(),
         ));
     }

     /**
      * Deletes a media entity.
      *
      * @Route("{id}", name="media_delete")
      * @Method("DELETE")
      */
     public function deleteAction(Request $request, Media $media)
     {
         $form = $this->createDeleteForm($media);
         $form->handleRequest($request);

         if ($form->isSubmitted() && $form->isValid()) {
             $em = $this->getDoctrine()->getManager();
             $em->remove($media);
             $em->flush();
         }

         return $this->redirectToRoute('media_index');
     }

     /**
      * Creates a form to delete a media entity.
      *
      * @param Project $project The media entity
      *
      * @return \Symfony\Component\Form\Form The form
      */
     private function createDeleteForm(Media $media)
     {
         return $this->createFormBuilder()
             ->setAction($this->generateUrl('media_delete', array('id' => $media->getId())))
             ->setMethod('DELETE')
             ->getForm()
         ;
     }
}
